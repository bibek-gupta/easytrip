<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeatInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_seat_info', function (Blueprint $table) {
            $table->increments('tbl_seat_info_id');
            $table->unsignedInteger('userId');
            $table->string('seat_id');
            $table->string('trip_id');
            $table->string('passenger_name');
            $table->string('passenger_gender');
            $table->integer('passenger_age');
            $table->integer('seat_price');
            $table->string('reserved_by');
            $table->foreign('userId')->references('id')->on('users');
            $table->foreign('trip_id')->references('trip_id')->on('tbl_bus_trip_schedule');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_seat_info');
    }
}
