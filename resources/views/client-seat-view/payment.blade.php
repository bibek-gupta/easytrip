@extends('layouts.app')
@section('title','payment')
@section('css')
<link href="{{asset('css/style.css')}}" rel="stylesheet">
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="payment-method">
            <div class="panel panel-primary">
                <div class="panel-heading">
                   <h2> Choose Your Payment Method</h2>
                </div>
                <div class="panel-body">
                    <div class="list-group">
                    <a href="#" class="list-group-item">
                        Visa Card
                    </a>
                    <a href="#" class="list-group-item">Debit Card</a>
                    <a href="#" class="list-group-item">Master Card</a>
                    <a href="#" class="list-group-item">Paypal</a>
                    <a href="#" class="list-group-item">E-sewa</a>
                    <a href="#" class="list-group-item"><button id="payment-button" class="btn btn-primary">Pay with Khalti</button>
</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
        var config = {
            // replace the publicKey with yours
            "publicKey": "test_public_key_ab028b05edf940e3bc8d2e44bb425a18",
            "productIdentity": "1234567890",
            "productName": "EasyTrip",
            "productUrl": "http://localhost/",
            "eventHandler": {
                onSuccess (payload) {
                    // hit merchant api for initiating verfication
                    $.ajax({
                        type: 'POST',
                        url: 'https://khalti.com/api/v2/payment/verify/',
                        headers:{
                            'Authorization':'test_public_key_ab028b05edf940e3bc8d2e44bb425a18',
                        },
                        payload:{
                            'token':'HRBUnjvreMFcSbNDPayFPj',
                            'amount':10,
                        },
                    });
                    console.log(payload);
                },
                onError (error) {
                    console.log(error);
                },
                onClose () {
                    console.log('widget is closing');
                }
            }
        };

        var checkout = new KhaltiCheckout(config);
        var btn = document.getElementById("payment-button");
        btn.onclick = function () {
            checkout.show({amount: 1000});
        }
    </script>
@endsection


@section('appjs')
    <script src="{{ asset('js/app.js') }}"></script>
@endsection

















