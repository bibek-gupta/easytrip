@extends('layouts.app')
@section('title','Easytrip')


@section('content')
    <div class="container">
        <div class="row">
            <div class=" table table-responsive">
                <table class="table table-stripped">
                    <thead class="bg-info">

                    <tr>
                        <th>Bus Name</th>
                        <th>Bus Number</th>
                        <th>Source</th>
                        <th>Destination</th>
                        <th>Booked For</th>
                        <th>Trip Id</th>
                        <th>Action</th>
                    </tr>
                    </thead>


                    @if(count($reservation)>0)
                        @foreach($reservation as $r)
                         <tr>
                            <td>{{$r->bus_name}}</td>
                            <td>{{$r->bus_number}}</td>
                            <td>{{$r->source}}</td>
                            <td>{{$r->destination}}</td>
                            <td>{{$r->passenger_name}}</td>
                            <td>{{$r->trip_id}}</td>
                            <td><a href="{{route('cancel-your-reservation',[$r->tbl_seat_info_id,$r->userId])}}">
                                <button class="btn btn-warning">Cancel</button></a>
                            </td>
                         </tr>  
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4" style="text-align: center"><h1>Sorry!!! You have not booked any seat till date</h1></td>
                        </tr>
                    @endif
                </table>


            </div>
        </div>
    </div>
@endsection


@section('appjs')
    <script src="{{ asset('js/app.js') }}"></script>
@endsection
