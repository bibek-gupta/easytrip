<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CancelReservation extends Model
{
    protected $table = 'cancelreservations';


    protected $fillable = [
        'id_for_cancelation'
    ];
}
