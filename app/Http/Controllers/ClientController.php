<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\CancelReservation;

class ClientController extends Controller
{
    public function index(){
        $reservation = DB::table('tbl_seat_info')
                                ->where('userId','=',Auth::id())
                                ->join('tbl_bus_trip_schedule','tbl_seat_info.trip_id','=','tbl_bus_trip_schedule.trip_id')
                                ->join('tbl_bus_details','tbl_bus_trip_schedule.tbl_bus_details_id','=','tbl_bus_details.tbl_bus_details_id')
                                ->get();
        // return $reservation->toArray();
        return view('myreservation',compact('reservation'));
   
    }


    public function requestCancellation($value,$user){
        $req = New CancelReservation;
        $req->id_for_cancelation = $value;
        $req->userId =  $user;
        $req->status =0;
        $req->save();
        return redirect('/myreservation');
        
    }
}
