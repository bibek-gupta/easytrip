<?php

namespace App\Http\Controllers;


use App\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $cityname=Route::all();
        $cityname  =   DB::table('cities')->orderBy('city','asc')
        ->get();
        $cityname2  =   DB::table('cities')->orderBy('city','desc')
        ->get();


        return view('home',compact('cityname','cityname2'));
    }
}
