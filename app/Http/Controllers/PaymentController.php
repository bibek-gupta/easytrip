<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\SeatInfo;
use Illuminate\Support\Facades\DB;
use Auth;
use Mail;
use App\Mail\verifyEmail;
use App\Mail\bookingConfirmation;

use Session;
Use App\User;

class PaymentController extends Controller
{
    public function index(Request $request){
        // return $request->toArray();


        for($i=0; $i<count($request->seatNumber);$i++){
            $seatinfo =SeatInfo::where('seat_id','=',$request->seatNumber[$i])
                                 ->where('trip_id','=',$request->tripID)
                                 ->get()->count();
            if($seatinfo!= 0)
            {
                return "The seat is already booked!!!";
            }           
            else{
                $seatinfo = new SeatInfo;
                $seatinfo->userId  = Auth::id();
                $seatinfo->seat_id = $request->seatNumber[$i];
                $seatinfo->trip_id = $request->tripID;
                $seatinfo->passenger_name = $request->passengerName[$i];
                $seatinfo->passenger_gender = $request->passengerGender[$i];
                $seatinfo->passenger_age = $request->passengerAge[$i];
                $seatinfo->seat_price = $request->seatPrice[$i];
                $seatinfo->reserved_by = "bibek";
                $seatinfo->save();

            }
            
        }
        return view('client-seat-view.payment');

    }
    public function confirmpayment(){
        Session::flash('status','Congrats!! You have succeefuly booked');
        $thisUser=User::findorFail(Auth::id());
         $this->sendEmail($thisUser);
         return redirect('/');
    }

    public function verifyEmailfirst()
    {
        return view('email.sendbookingconfirmation');
    }
    public function sendEmail($thisUser){
        Mail::to($thisUser['email'])->send(new bookingConfirmation($thisUser));
    }
}
