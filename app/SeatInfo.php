<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeatInfo extends Model
{
    protected $table = 'tbl_seat_info';
    protected $primaryKey='tbl_seat_info_id';


    protected $fillable = [
        'userId','seat_id', 'trip_id','passenger_name','passenger_gender','passenger_age','seat_price','reserved_by'
    ];

}
